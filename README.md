# Files Merger

File merger app is an app that merges two files.

## Installation

Run main.py file

## Usage

The files first_names and last_names are input files for the app. If you want to add your own just replace them with the same name.

There is also an helper create_file.py that a number amount of rows and creates both files. Just run it and it will create corresponding files.

## Description

So the general idea was pretty simple. Open two files merge them by a single column, in this case the ID and create a merged file with full names and IDs and sort it.

With a bit of brainstorming the real problem was the potential file size. So I could not just simply open them, inject them to the memory and map them to any sort of data structure or do any kind of in memory sorting.

After doing a little bit of digging on how to solve this problem, from pandas, dask, linecache and many other stuff, I decided to do it in three steps and with pretty much with basic lib from python.

Phase one:
I sorted one of the files by ID, since it to is a potentially very large file, I implemented external merge sorting using heaps. Not the heapd in python but I made it from an array.

So the first file is chunked, then merge sorted, then chunked again and additionally mapped to each file handler. Now the mapping is done so that file is chunked in to a particular size, optimally to the size of potentially physical memory and every first line inside the file is saved. The reason for that is the next step and that is merging two files.

Phase two:
Second file is opened with memory mapping object and iterated line by line. With every line, ID of the given line is extracted. Since we mapped the first file and we know each file starting line ID, now it is only matter of extracting the right file handler. That is being done by the bisect function which will fetch the next lowest file with corresponding ID.

For more info check the helpers.py the fetch_file_handler method.

Phase three:
When we merged two files and now we have unsorted full names list which is then again run thru the external merge sort and sorted according to its IDs.

And thats pretty much it.

There is 150% chance that I have not thought about all the edge cases, and I am aware of that since I was a bit short on time (I already took to long to solve this anyway) and also coupled with not really having any experience handling files, especially big onesm there is probably more stuff I could do better.

Also note that there is a /temp folder, which sole purpose is to provide a spot for temporary folders being generated via sorting. The last step is the actual clean up of that folder but that can be disabled with the comment of a single line.

Setup.py is inside the project, but is not really necessary.

## Bulit with

* [Python](https://www.python.org/) - Python programming language
