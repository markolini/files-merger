from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()


setup(
    name='file-merger',
    version='0.1.0',
    description='Little app that merges data from two separate files into a single file',
    long_description=readme,
    author='Marko Kos',
    author_email='marko.kos89@gmail.com'
)
