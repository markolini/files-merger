from .file_parser import merge_files
from .sorter import ExternalMergeSorter
from .helpers import cleanup_temp_dir


def start_app():
    temp_file_size = 30

    names_file = 'first_names'
    surnames_file = 'last_names'

    sorted_first_names_file = 'sorted_first_names'

    unsorted_full_names_file = 'unsorted_full_names'
    sorted_full_names_file = 'sorted_full_names'

    first_phase = ExternalMergeSorter()
    second_phase = ExternalMergeSorter()
    thrid_phase = ExternalMergeSorter()

    # first phase we sort one of the files
    first_phase.split_and_sort(
        names_file, sorted_first_names_file, temp_file_size)

    # then we split the sorted files again and map the files to corresponding ids
    second_phase.split_files(sorted_first_names_file, temp_file_size)

    # we merge the files into one single big file
    merge_files(second_phase.sorted_temp_file_mapper_list,
                surnames_file, unsorted_full_names_file)

    # and last phase is to sort the merged file
    thrid_phase.split_and_sort(
        unsorted_full_names_file, sorted_full_names_file, temp_file_size)

    # disable this if you want to see what is inside /temp directory
    cleanup_temp_dir()
