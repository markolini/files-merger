from .helpers import fetch_file_handler, get_id

import mmap

""" The function uses mmap to open a very large file. Here we have a map of file handlers (mapped_file_handlers) 
    for the first file which are sorted by ID. The mapped_file_handlers property is a map, its key is the first ID of the first
    line of text and the value is a pointer to the chunked temporary file. Although the function has nested loops, the idea is
    to search only """


def merge_files(mapped_file_handlers, input_filename, output_file_name):
    try:
        with open(output_file_name, 'w') as output_file, open(input_filename, 'r') as input_file:
            with mmap.mmap(input_file.fileno(), length=0, access=mmap.ACCESS_READ) as input_mmap_obj:
                for input_file_line in iter(input_mmap_obj.readline, b''):
                    input_file_id = get_id(input_file_line.decode('utf-8'))

                    file_mapper = fetch_file_handler(
                        input_file_id, mapped_file_handlers)

                    temp_file = file_mapper.file_handler

                    for temp_file_line in temp_file:
                        temp_file_id = get_id(temp_file_line)

                        if input_file_id == temp_file_id:
                            output_file.write("%s\n" % create_row(
                                input_file_id, temp_file_line, input_file_line))
                            temp_file.seek(0)
                            break
    except FileNotFoundError as err:
        print('Error opening file  %s - %s.' % (err.filename, err.strerror))


def create_row(id, name_line, surname_line):
    name = name_line.split()[0]
    surname = surname_line.decode('utf-8').split()[0]

    return name + ' ' + surname + ' ' + id
