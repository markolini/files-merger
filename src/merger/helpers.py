from .models import FileMapper
from bisect import bisect

import os
import glob


def cleanup_temp_dir():
    dir = get_current_dir() + '/temp'
    file_list = glob.glob(os.path.join(dir, "*"))

    for file in file_list:
        os.remove(file)


def get_current_dir():
    return os.getcwd()


def get_id(string_line):
    return string_line.split()[-1].strip()


def fetch_file_handler(id, mapped_file_handlers):
    index = bisect(mapped_file_handlers, FileMapper(int(id)))
    return mapped_file_handlers[index - 1]


def heapify(arr, i, n):
    i = int(i)
    left = int(2 * i) + 1
    right = int(2 * i) + 2

    if left < n and arr[left].id < arr[i].id:
        smallest = left
    else:
        smallest = i

    if right < n and arr[right].id < arr[smallest].id:
        smallest = right

    if i != smallest:
        (arr[i], arr[smallest]) = (arr[smallest], arr[i])
        heapify(arr, smallest, n)


def construct_heap(arr):
    l = len(arr) - 1
    mid = l / 2
    while mid >= 0:
        heapify(arr, mid, l)
        mid -= 1
