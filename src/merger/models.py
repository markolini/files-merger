class Node:
    """ Single heap node
       @params
               id           The id used for sorting inside heap
               row          The actual value to be stored in heap
               file_handler The file handler of the file that stores fullname and id """

    def __init__(
            self,
            id,
            row,
            file_mapper,
    ):
        self.id = id
        self.row = row
        self.file_mapper = file_mapper


class FileMapper:
    """ Convinience object for easier file mapping
       @params
               id           The id used for looking up what ID is in the first row on given file
               file_handler The file handler of the file that stores name and id """
    def __init__(self, id, file_handler=None):
        self.id = id
        self.file_handler = file_handler

    def __lt__(self, other):
        return self.id < other.id
