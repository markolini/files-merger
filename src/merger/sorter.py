from .models import Node, FileMapper
from .helpers import get_id, construct_heap, heapify, get_current_dir

import tempfile
import sys


class ExternalMergeSorter:
    """ Splits the large file into small files then sorts them into to small files """
    """ @params
           sorted_temp_file_handler_list - List of all file handlers to all temp files formed by splitting large files
    """

    def __init__(self):
        self.sorted_temp_file_mapper_list = []

    """ Move first element of all files to a min heap. The Heap has now the smallest element.
        Moves that element from heap to a file. Get the file handler of that element. Read the next element using the same file handler.
        If next file element is empty, mark it as INT_MAX. Moves it to heap. Again Heapify.
        Continue this until all elements of heap is INT_MAX or all the smaller files have read fully. """

    def merge_sorted_temp_files(self, output_filename):
        list = []
        with open(output_filename, 'w') as output_file:
            for file_mapper in self.sorted_temp_file_mapper_list:

                item = file_mapper.file_handler.readline().strip()
                list.append(Node(int(get_id(item)), item,
                            file_mapper.file_handler))

            construct_heap(list)

            while True:
                min = list[0]

                if min.id == sys.maxsize:
                    break

                file_handler = min.file_mapper
                item = file_handler.readline().strip()
                node_id = 0

                if not item:
                    node_id = sys.maxsize
                else:
                    node_id = int(get_id(item))

                list[0] = Node(node_id, item, file_handler)
                output_file.write("%s\n" % min.row)
                heapify(list, 0, len(list))

    """ Splits a large file into smaller files, sorts them and stores them to temporary files """

    def split_files(self, input_filename, temp_file_size):
        buffer = []
        size = 0
        try:
            with open(input_filename) as file:
                for line in file:
                    if not line:
                        break

                    buffer.append(line)
                    size += 1

                    if size % temp_file_size == 0:
                        buffer = sorted(
                            buffer, key=lambda no: int(get_id(no)))
                        temporary_file = tempfile.NamedTemporaryFile(mode='w+', dir=get_current_dir()
                                                                     + '/temp', delete=False)
                        temporary_file.writelines(buffer)
                        temporary_file.seek(0)

                        # read first line in the temp file
                        temp_file_first_line = temporary_file.readline()

                        # get the id of the first line in the temp file
                        first_line_id = get_id(temp_file_first_line)

                        # we map the file handlers in a map with key being the first id in the file
                        self.sorted_temp_file_mapper_list.append(
                            FileMapper(int(first_line_id), temporary_file))
                        buffer = []
        except FileNotFoundError as err:
            print('Error opening file  %s - %s.' %
                  (err.filename, err.strerror))

    def split_and_sort(self, input_file_name, output_file_name, temp_file_size):
        self.split_files(input_file_name, temp_file_size)
        self.merge_sorted_temp_files(output_file_name)
